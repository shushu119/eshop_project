
var menutree = (function(){
	/**
	 * 展示菜单节点树
	 * 参数：dom 展示树位于页面那个dom节点中  string   示例： ‘#testdemo’
	 *     listdata 数据				  object   示例件页面最下方*1
	 * */
	function showTree(dom,listdata){
		layui.use(['tree', 'layer'], function(){
			var layer = layui.layer;
			var tree = layui.tree;
			  tree.render({
			    elem: dom
			    ,data: listdata
			    ,onlyIconControl:true //是否仅允许节点左侧图标控制展开收
			    ,showCheckbox: false  //是否显示复选框
			    ,edit:['add', 'update', 'del'] //是否开启节点的操作图标。默认 false。若为 true，则默认显示“改删”图标,或edit: ['add', 'update', 'del']
			    ,id: 'demoId1'
			    ,isJump: true //是否允许点击节点时弹出新窗口跳转
			    ,click: function(obj){
			      var data = obj.data;  //获取当前点击的节点数据
			      layer.msg('状态：'+ obj.state + '<br>节点数据：' + JSON.stringify(data));
			    }
			    ,text: {
			       defaultNodeName: '未命名' //节点默认名称
				  ,none: '无数据' //数据为空时的提示文本
					} 
			    ,operate:function(obj){
			    	var type = obj.type; //得到操作类型：add、edit、del
			        var data = obj.data; //得到当前节点的数据
			        var elem = obj.elem; //得到当前节点元素
			        console.log(type);
			        console.log(data);
			        
			        //Ajax 实时操作
			        if(type === 'add'){
			        	var primary_key = '';
			        	ajaxmoudle.ajaxRequest('POST','/addMenu',{name:'未命名',pid:data.id},function(info){
			        		if(info.status == 0){
			        			primary_key = info.data;
			        			winalert.msg(info.info);
			        		}
			        		console.log(info.info);
			        	},false);
			        	return primary_key;
			        } else if(type === 'update'){ //修改节点
			        	ajaxmoudle.ajaxRequest('POST','/editMenu',{name:data.title,id:data.id},function(info){
			        		winalert.msg(info.info);
			        	});
			        } else if(type === 'del'){ //删除节点
			        	ajaxmoudle.ajaxRequest('POST','/delMenu',{id:data.id},function(info){
			        		winalert.msg(info.info);
			        	});
			        };
			        
			    }
			  });
	     });
	}
	
	return {
		showTree:showTree
	};
	
})();
/**
 * *1：data 示例：
 * [
    {
        "title":"系统设置",
        "id":1,
        "field":null,
        "href":null,
        "spread":null,
        "checked":null,
        "disabled":null,
        "children":[
            {
                "title":"修改密码",
                "id":3,
                "field":null,
                "href":null,
                "spread":null,
                "checked":null,
                "disabled":null,
                "children":null,
                "pid":1
            },
            Object{...}
        ],
        "pid":null
    },
    Object{...}
]
 * 
 */