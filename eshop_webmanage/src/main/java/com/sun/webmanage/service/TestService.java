package com.sun.webmanage.service;

import java.util.List;

import com.sun.webmanage.model.TbContentCategory;

public interface TestService {

	List<TbContentCategory> listData();
}
